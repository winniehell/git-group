#!/usr/bin/env bash

set -o errexit
set -o nounset

script_directory=$(dirname "$0")
root_directory=$1

find "${root_directory}" -type d -name .git -print0 | (
  while IFS= read -r -d $'\0' repository; do
    if "${script_directory}/git-check-head-outdated.sh" "${repository}"; then
      echo "${repository}: is already up to date"
      continue
    fi

    (cd "${repository}" && git pull)
  done
)
