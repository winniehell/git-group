#!/usr/bin/env bash

# checks whether current branch of passed repository matches remote tracking branch without modifying repository

set -o errexit
set -o nounset

repository=$1

cd "${repository}"
current_branch=$(git rev-parse --abbrev-ref HEAD)
remote_name=$(git config --get "branch.${current_branch}.remote")
remote_branch=$(git config --get "branch.${current_branch}.merge")
remote_sha=$(git ls-remote "${remote_name}" "${remote_branch}" | cut -d$'\t' -f1)

# check if commit exists locally
git cat-file -e "${remote_sha}"

# check if local branch contains commit
git rev-list "${current_branch}" | grep --quiet "${remote_sha}"
